package com.sprhib.controller;

import com.sprhib.model.User;
import com.sprhib.model.UserPage;
import com.sprhib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView adduserPage() {
        ModelAndView modelAndView = new ModelAndView("add-user-form");
        modelAndView.addObject("user", new User());
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addinguser(@ModelAttribute User user) {

        ModelAndView modelAndView = new ModelAndView("home");
        user.setCreatedDate(new Date());
        userService.addUser(user);

        String message = "User was successfully added.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value = "/list")
    public ModelAndView listPage() {
        ModelAndView modelAndView = new ModelAndView("list-of-users");

        List<User> users = userService.getUsers();
        modelAndView.addObject("users", users);

        return modelAndView;
    }


    @RequestMapping(value = "/listPage/{page}")
    public ModelAndView listPage(final @PathVariable Integer page) {
        ModelAndView modelAndView = new ModelAndView("page-of-users");

        final UserPage userPage = userService.getUsers(page);
        modelAndView.addObject("users", userPage.getUsers());
        modelAndView.addObject("pageNumber", userPage.getCurrentPageNumber());
        modelAndView.addObject("lastPageNumber", userPage.getLastPageNumber() - 1);
        return modelAndView;
    }

    @RequestMapping(value = "/listByName/{name}")
    public ModelAndView list(final @PathVariable String name) {
        ModelAndView modelAndView = new ModelAndView("list-of-users");

        List<User> users = userService.getUsers(name);
        modelAndView.addObject("users", users);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView edituserPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("edit-user-form");
        User user = userService.getUser(id);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView edditinguser(@ModelAttribute User user, @PathVariable Integer id) {

        ModelAndView modelAndView = new ModelAndView("home");

        userService.updateUser(user);

        String message = "User was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteuser(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("home");
        userService.deleteUsers(id);
        String message = "User was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

}
