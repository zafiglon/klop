package com.sprhib.dao;

import com.sprhib.model.User;
import com.sprhib.model.UserPage;

import java.util.List;

public interface UserDao {
	
	public void add(User user);
	public void update(User user);
	public User list(int id);
	public void delete(int id);
	public List<User> list(String name);
	public List<User> list();
	public UserPage listPage(Integer page);

}
