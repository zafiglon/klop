package com.sprhib.dao;

import com.sprhib.model.User;
import com.sprhib.model.UserPage;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDao {

    public static final int PAGE_SIZE = 10;
    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void add(User user) {
        getCurrentSession().save(user);
    }

    public void update(User user) {
        User userToUpdate = list(user.getId());
        userToUpdate.setName(user.getName());
        userToUpdate.setAge(user.getAge());
        userToUpdate.setAdmin(user.getAdmin());
        getCurrentSession().update(userToUpdate);
    }

    public User list(int id) {
        User user = (User) getCurrentSession().get(User.class, id);
        return user;
    }

    public void delete(int id) {
        User user = list(id);
        if (user != null)
            getCurrentSession().delete(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> list(final String name) {
        final Query query = getCurrentSession().createQuery("from User where name = :name");
        query.setParameter("name", name);
        return query.list();
    }


    @SuppressWarnings("unchecked")
    public UserPage listPage(final Integer page) {
        final Query query = getCurrentSession().createQuery("from User");
        query.setMaxResults(10);
        query.setFirstResult(page * 10);
        final UserPage userPage = new UserPage();
        final List list = query.list();
        String countQ = "Select count (f.id) from User f";
        Query countQuery = getCurrentSession().createQuery(countQ);
        Long countResults = (Long) countQuery.uniqueResult();
//Last Page
        int lastPageNumber = (int) ((countResults / PAGE_SIZE) + 1);
        userPage.setUsers(list);
        userPage.setCurrentPageNumber(page);
        userPage.setLastPageNumber(lastPageNumber);
        return userPage;
    }

    @SuppressWarnings("unchecked")
    public List<User> list() {
        final Query query = getCurrentSession().createQuery("from User");
        return query.list();
    }

}
