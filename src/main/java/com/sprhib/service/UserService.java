package com.sprhib.service;

import com.sprhib.model.User;
import com.sprhib.model.UserPage;

import java.util.List;

public interface UserService {
	
	public void addUser(User user);
	public void updateUser(User user);
	public User getUser(int id);
	public void deleteUsers(int id);
	public List<User> getUsers(String name);
	public List<User> getUsers();
	public UserPage getUsers(Integer page);

}
