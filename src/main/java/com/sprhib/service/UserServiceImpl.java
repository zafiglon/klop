package com.sprhib.service;

import com.sprhib.dao.UserDao;
import com.sprhib.model.User;
import com.sprhib.model.UserPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;

	public void addUser(User user) {
		userDao.add(user);
	}

	public void updateUser(User user) {
		userDao.update(user);
	}

	public User getUser(int id) {
		return userDao.list(id);
	}

	public void deleteUsers(int id) {
		userDao.delete(id);
	}

	public List<User> getUsers(String name) {
		return userDao.list(name);
	}
	public List<User> getUsers() {
		return userDao.list();
	}
	public UserPage getUsers(final Integer page) {
		return userDao.listPage(page);
	}

}
